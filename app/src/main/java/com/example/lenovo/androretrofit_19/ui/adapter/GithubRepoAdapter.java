package com.example.lenovo.androretrofit_19.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lenovo.androretrofit_19.R;
import com.example.lenovo.androretrofit_19.api.model.GithubRepo;

import java.util.List;

/**
 * Created by Ari Mahardika (arimahardika.an@gmail.com) on 1/30/2018.
 */

public class GithubRepoAdapter extends ArrayAdapter<GithubRepo> {

    private Context context;
    private List<GithubRepo> values;

    public GithubRepoAdapter(Context context, List<GithubRepo> values) {
        super(context, R.layout.list_item_pagination, values);
        this.context = context;
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.list_item_pagination, parent, false);
        }

        TextView textView = row.findViewById(R.id.list_item_pagination_text);

        GithubRepo item = values.get(position);
        String message = item.getName();
        textView.setText(message);

        return row;
    }
}
