package com.example.lenovo.androretrofit_19.api.service;

import com.example.lenovo.androretrofit_19.api.model.GithubRepo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ari Mahardika (arimahardika.an@gmail.com) on 1/30/2018.
 */

public interface GithubClient {

    @GET("/users/{user}/repos")
    Call<List<GithubRepo>> reposForUser(@Path("user") String user);
}
